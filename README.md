#AlertHit PHP

Official php-sdk for AlertHit API.
AlertHIT - VISA CERTIFIED VMPI FACILITATOR.
                                   
**Features:**

* prepared models for request/response;
* Low-level client for different operations (validate response, create test alert for integration)

**Documentation:**

https://docs.alerthit.com/docs

**Installation:**

`composer require alerthit/php-sdk`


**Quick start:**

```php
<?php

use Alerthit\Api;
use Alerthit\Model\Request\AlertModel;

// Get body request
$content = '';
$alertModel = new AlertModel(json_decode($content, true));

// Get parameters for matching
$alertModel->getRequestData()->getTransactionId();

$alertModel->getRequestData()->getAuthorizationCode();
$alertModel->getRequestData()->getPersonalAccountNumber();

$alertModel->getRequestData()->getTransactionDate();
$alertModel->getRequestData()->getTransactionAmount()->getValue();
$alertModel->getRequestData()->getTransactionAmount()->getCurrency();

############################################################

$api = new Api('requestIdentifier', 'privateKey', 'https://api.alerthit.com/api/v1/');

// Generate test alert
$api->createTestAlert([]);

// Validate response
$api->validateResponse([]);

```