<?php

use Alerthit\Api;
use Alerthit\Model\Request\AlertModel;

// Get body request
$content = '';
$alertModel = new AlertModel(json_decode($content, true));

// Get parameters for matching
$alertModel->getRequestData()->getTransactionId();

$alertModel->getRequestData()->getAuthorizationCode();
$alertModel->getRequestData()->getPersonalAccountNumber();

$alertModel->getRequestData()->getTransactionDate();
$alertModel->getRequestData()->getTransactionAmount()->getValue();
$alertModel->getRequestData()->getTransactionAmount()->getCurrency();

$api = new Api('requestIdentifier', 'privateKey', 'https://api.alerthit.com/api/v1/');

// Generate test alert
$api->createTestAlert([]);

// Validate response
$api->validateResponse([]);


