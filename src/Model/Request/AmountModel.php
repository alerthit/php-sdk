<?php namespace Alerthit\Model\Request;

class AmountModel extends \Alerthit\Model\AmountModel
{
    /**
     * @param array|null $data
     */
    public function __construct($data)
    {
        parent::__construct(
            isset($data['value']) ? $data['value'] : null,
            isset($data['currency']) ? $data['currency'] : null
        );
    }
}
