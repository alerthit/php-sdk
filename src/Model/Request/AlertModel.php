<?php namespace Alerthit\Model\Request;

class AlertModel
{
    /** @var string|null */
    protected $alertId;

    /** @var RequestInfoModel|null */
    protected $requestHeader;

    /** @var PurchaseModel|null */
    protected $requestData;

    public function __construct($data)
    {
        $this->alertId = isset($data['alertId']) ? $data['alertId'] : null;
        $this->requestHeader = new RequestInfoModel($data['requestHeader'] ?? []);
        $this->requestData = new PurchaseModel($data['requestData'] ?? []);
    }

    /**
     * @return string|null
     */
    public function getAlertId()
    {
        return $this->alertId;
    }

    /**
     * @param string|null $alertId
     */
    public function setAlertId($alertId)
    {
        $this->alertId = $alertId;
    }

    /**
     * @return RequestInfoModel|null
     */
    public function getRequestHeader()
    {
        return $this->requestHeader;
    }

    /**
     * @param RequestInfoModel|null $requestHeader
     */
    public function setRequestHeader($requestHeader)
    {
        $this->requestHeader = $requestHeader;
    }

    /**
     * @return PurchaseModel|null
     */
    public function getRequestData()
    {
        return $this->requestData;
    }

    /**
     * @param PurchaseModel|null $requestData
     */
    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;
    }
}