<?php namespace Alerthit\Model\Request;

class PurchaseModel
{
    const CALL_TYPE_INIT_REFUND = 0;
    const CALL_TYPE_INQUIRY = 1;
    const CALL_TYPE_FRAUD_NOTIFICATION = 2;
    const CALL_TYPE_DISPUTE_NOTIFICATION = 3;
    const CALL_TYPE_STOP_PAYMENT = 4;
    const CALL_TYPE_EXCEPTION_FILE_LISTING = 5;
    const CALL_TYPE_ACCOUNT_UPDATE = 6;
    const CALL_TYPE_RESOLVED = 8;

    const CALL_TYPES = [
        self::CALL_TYPE_INIT_REFUND,
        self::CALL_TYPE_INQUIRY,
        self::CALL_TYPE_FRAUD_NOTIFICATION,
        self::CALL_TYPE_DISPUTE_NOTIFICATION,
        self::CALL_TYPE_STOP_PAYMENT,
        self::CALL_TYPE_EXCEPTION_FILE_LISTING,
        self::CALL_TYPE_ACCOUNT_UPDATE,
        self::CALL_TYPE_RESOLVED,
    ];

    /** @var string|null */
    protected $authorizationCode;

    /** @var string|null */
    protected $merchantReferenceNumber;

    /** @var string|null */
    protected $rrn;

    /** @var string|null */
    protected $transactionDate;

    /** @var string|null */
    protected $transactionId;

    /** @var string|null */
    protected $purchaseIdentifier;

    /** @var string|null */
    protected $authorizationDateTime;

    /** @var string|null */
    protected $cardAcceptorId;

    /** @var string|null */
    protected $transactionType;

    /** @var string|null */
    protected $personalAccountNumber;

    /** @var AmountModel|null */
    protected $transactionAmount;

    /** @var string|null */
    protected $arn;

    public function __construct($data)
    {
        $this->authorizationCode = isset($data['authorizationCode']) ? $data['authorizationCode'] : null;
        $this->merchantReferenceNumber = isset($data['merchantReferenceNumber']) ? $data['merchant¬ReferenceNumber'] : null;
        $this->rrn = isset($data['rrn']) ? $data['rrn'] : null;
        $this->transactionDate = isset($data['transactionDate']) ? $data['transactionDate'] : null;
        $this->transactionId = $data['transactionId'] ?? $data['transactionID'] ?? null;
        $this->purchaseIdentifier = isset($data['purchaseIdentifier']) ? $data['purchaseIdentifier'] : null;
        $this->authorizationDateTime = isset($data['authorizationDateTime']) ? $data['authorizationDateTime'] : null;
        $this->cardAcceptorId = isset($data['cardAcceptorId']) ? $data['cardAcceptorId'] : null;
        $this->transactionType = isset($data['transactionType']) ? $data['transactionType'] : null;
        $this->personalAccountNumber = isset($data['personalAccountNumber']) ? $data['personalAccountNumber'] : null;
        $this->transactionAmount = new AmountModel($data['transactionAmount'] ?? []);
        $this->arn = isset($data['arn']) ? $data['arn'] : null;
    }

    /**
     * @return string|null
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * @param string|null $authorizationCode
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;
    }

    /**
     * @return string|null
     */
    public function getMerchantReferenceNumber()
    {
        return $this->merchantReferenceNumber;
    }

    /**
     * @param string|null $merchantReferenceNumber
     */
    public function setMerchantReferenceNumber($merchantReferenceNumber)
    {
        $this->merchantReferenceNumber = $merchantReferenceNumber;
    }

    /**
     * @return string|null
     */
    public function getRrn()
    {
        return $this->rrn;
    }

    /**
     * @param string|null $rrn
     */
    public function setRrn($rrn)
    {
        $this->rrn = $rrn;
    }

    /**
     * @return string|null
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @param string|null $transactionDate
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;
    }

    /**
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param string|null $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return string|null
     */
    public function getPurchaseIdentifier()
    {
        return $this->purchaseIdentifier;
    }

    /**
     * @param string|null $purchaseIdentifier
     */
    public function setPurchaseIdentifier($purchaseIdentifier)
    {
        $this->purchaseIdentifier = $purchaseIdentifier;
    }

    /**
     * @return string|null
     */
    public function getAuthorizationDateTime()
    {
        return $this->authorizationDateTime;
    }

    /**
     * @param string|null $authorizationDateTime
     */
    public function setAuthorizationDateTime($authorizationDateTime)
    {
        $this->authorizationDateTime = $authorizationDateTime;
    }

    /**
     * @return string|null
     */
    public function getCardAcceptorId()
    {
        return $this->cardAcceptorId;
    }

    /**
     * @param string|null $cardAcceptorId
     */
    public function setCardAcceptorId($cardAcceptorId)
    {
        $this->cardAcceptorId = $cardAcceptorId;
    }

    /**
     * @return string|null
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param string|null $transactionType
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return string|null
     */
    public function getPersonalAccountNumber()
    {
        return $this->personalAccountNumber;
    }

    /**
     * @param string|null $personalAccountNumber
     */
    public function setPersonalAccountNumber($personalAccountNumber)
    {
        $this->personalAccountNumber = $personalAccountNumber;
    }

    /**
     * @return AmountModel|null
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * @param AmountModel|null $transactionAmount
     */
    public function setTransactionAmount($transactionAmount)
    {
        $this->transactionAmount = $transactionAmount;
    }

    /**
     * @return string|null
     */
    public function getArn()
    {
        return $this->arn;
    }

    /**
     * @param string|null $arn
     */
    public function setArn($arn)
    {
        $this->arn = $arn;
    }
}
