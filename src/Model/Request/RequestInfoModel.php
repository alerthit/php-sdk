<?php namespace Alerthit\Model\Request;

class RequestInfoModel
{
    /** @var array|null */
    protected $callTypes;

    /**
     * @param array|null $data
     */
    public function __construct($data)
    {
        $this->callTypes = isset($data['callType']) ? $data['callType'] : null;
    }

    /**
     * @return array|null
     */
    public function getCallTypes()
    {
        return $this->callTypes;
    }

    /**
     * @param array|null $callTypes
     */
    public function setCallTypes($callTypes)
    {
        $this->callTypes = $callTypes;
    }

    /**
     * @return bool
     */
    public function isCallTypeInitRefund()
    {
        return (bool) in_array(PurchaseModel::CALL_TYPE_INIT_REFUND, $this->getCallTypes());
    }

    /**
     * @return bool
     */
    public function isCallTypeInquiry()
    {
        return (bool) in_array(PurchaseModel::CALL_TYPE_INQUIRY, $this->getCallTypes());
    }

    /**
     * @return bool
     */
    public function isCallTypeResolved()
    {
        return (bool) in_array(PurchaseModel::CALL_TYPE_RESOLVED, $this->getCallTypes());
    }
}
