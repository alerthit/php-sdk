<?php namespace Alerthit\Model\Response;

use Alerthit\Model\Response\Purchase\CreditInquiry;

class PurchaseInfoModel
{
    const FRAUD_REPORT_GOODS_DISTRIBUTION_CANCELLED = 1;
    const FRAUD_REPORT_GOODS_DISPATCHED_AND_IN_TRANSIT = 2;
    const FRAUD_REPORT_SERVICE_NOW_CANCELLED = 3;
    const FRAUD_REPORT_SERVICE_REDEEMED = 4;
    const FRAUD_REPORT_ACKNOWLEDGED = 5;
    const FRAUD_REPORT_TRANSACTION_REVERSED = 6;
    const FRAUD_REPORT_ERROR = 7;

    /** @var PurchaseModel|null */
    protected $purchaseInformationResponseData;

    /** @var int|null */
    protected $fraudReportNoticationResponse;

    /** @var string|null */
    protected $systemFraudReport;

    /**
     * For call type "Credit inquiry"
     *
     * @var CreditInquiry|null
     */
    protected $creditInquiryResponse;

    /**
     * @return PurchaseModel|null
     */
    public function getPurchaseInformationResponseData()
    {
        return $this->purchaseInformationResponseData;
    }

    /**
     * @param PurchaseModel|null $purchaseInformationResponseData
     */
    public function setPurchaseInformationResponseData($purchaseInformationResponseData)
    {
        $this->purchaseInformationResponseData = $purchaseInformationResponseData;
    }

    /**
     * @return int|null
     */
    public function getFraudReportNoticationResponse()
    {
        return $this->fraudReportNoticationResponse;
    }

    /**
     * @param int|null $fraudReportNoticationResponse
     */
    public function setFraudReportNoticationResponse($fraudReportNoticationResponse)
    {
        $this->fraudReportNoticationResponse = $fraudReportNoticationResponse;
    }

    /**
     * @return string|null
     */
    public function getSystemFraudReport()
    {
        return $this->systemFraudReport;
    }

    /**
     * @param string|null $systemFraudReport
     */
    public function setSystemFraudReport($systemFraudReport)
    {
        $this->systemFraudReport = $systemFraudReport;
    }

    /**
     * @return CreditInquiry|null
     */
    public function getCreditInquiryResponse()
    {
        return $this->creditInquiryResponse;
    }

    /**
     * @param CreditInquiry|null $creditInquiryResponse
     */
    public function setCreditInquiryResponse($creditInquiryResponse)
    {
        $this->creditInquiryResponse = $creditInquiryResponse;
    }
}
