<?php namespace Alerthit\Model\Response;

use Alerthit\Model\Response\Purchase\AddressModel;
use Alerthit\Model\Response\Purchase\CreditInquiry;
use Alerthit\Model\Response\Purchase\CreditReimbursementInformationList;
use Alerthit\Model\Response\Purchase\DigitalReceiptModel;
use Alerthit\Model\Response\Purchase\LocationModel;
use Alerthit\Model\Response\Purchase\ShippingInfoModel;
use DateTime;

class PurchaseModel
{
    /** @var bool|null */
    protected $cardCVV2ValidationAtRegistration;

    /** @var DateTime|null */
    protected $dateContractExecution;

    /** @var int|null */
    protected $additionalRelatedPurchases;

    /** @var string|null */
    protected $loyaltyNameRegistered;

    /** @var string|null */
    protected $flightManifestName;

    /** @var string|null */
    protected $merchantReferenceNumber;

    /** @var string|null */
    protected $carRentalPickUpLocation;

    /** @var DateTime|null */
    protected $dateOfLastUtilization;

    /** @var bool|null */
    protected $cardCVV2ValidationAtPurchase;

    /** @var string|null */
    protected $simDetails;

    /** @var string|null */
    protected $linkToItemSold;

    /** @var DateTime|null */
    protected $downloadCompletionDateTime;

    /** @var string|null */
    protected $recipientCustomerName;

    /** @var string|null */
    protected $deliveryService;

    /** @var float|null */
    protected $cardholderPriorVisitCount;

    /** @var string|null */
    protected $websiteLink;

    /** @var string|null */
    protected $trackingNumber;

    /** @var CreditReimbursementInformationList|null */
    protected $creditReimbursementInformationList;

    /** @var bool|null */
    protected $assuredCredit;

    /** @var string|null */
    protected $conditionofGoods;

    /** @var string|null */
    protected $carRentalDropoffLocation;

    /** @var string|null */
    protected $authenticationConducted;

    /** @var string|null */
    protected $deviceName;

    /** @var string|null */
    protected $loyaltyNumberRegistered;

    /** @var string|null */
    protected $hotelRoomStayDuration;

    /** @var string|null */
    protected $customerName;

    /** @var string|null */
    protected $hotelRoomType;

    /** @var DateTime|null */
    protected $downloadDateTime;

    /** @var string|null */
    protected $packingSlip;

    /** @var string|null */
    protected $additionalRelatedDisputes;

    /** @var string|null */
    protected $contactUs;

    /** @var DateTime|null */
    protected $carRentalPickUpDate;

    /** @var AddressModel|null */
    protected $loyaltyAddressRegistered;

    /** @var int|null */
    protected $customerNameDuration;

    /** @var ShippingInfoModel|null */
    protected $shippingInfo;

    /** @var LocationModel|null */
    protected $storeLocation;

    /** @var DateTime|null */
    protected $contractFirstPaymentDate;

    /** @var bool|null */
    protected $contractFirstPaymentDisputed;

    /** @var AddressModel|null */
    protected $address;

    /** @var string|null */
    protected $passengerName1;

    /** @var string|null */
    protected $passengerName2;

    /** @var string|null */
    protected $passengerName3;

    /** @var string|null */
    protected $passengerName4;

    /** @var string|null */
    protected $passengerName5;

    /** @var @var DateTime|null */
    protected $downloadStartDateTime;

    /** @var string|null */
    protected $relationshipToAccountholder;

    /** @var string|null */
    protected $reservationNumber;

    /** @var int|null */
    protected $emailAccountHistory;

    /** @var int|null */
    protected $countOfPurchasesSinceRegistration;

    /** @var @var DateTime|null */
    protected $passwordLastUpdatedDateTime;

    /** @var @var DateTime|null */
    protected $flightManifestDateOfBirth;

    /** @var string|null */
    protected $specialInstructions;

    /** @var string|null */
    protected $carRentalVehicleType;

    /** @var string|null */
    protected $travelInsurance;

    /** @var bool|null */
    protected $travelUtilizationIndicator;

    /** @var @var DateTime|null */
    protected $dateofReservation;

    /** @var string|null */
    protected $deliveryStatus;

    /** @var string|null */
    protected $communications;

    /** @var string|null */
    protected $deviceType;

    /** @var string|null */
    protected $deviceId;

    /** @var string|null */
    protected $itinerary;

    /** @var AddressModel|null */
    protected $deliveryAddress;

    /** @var int|null */
    protected $countOfDisputedPurchasesSinceRegistration;

    /** @var int|null */
    protected $monthsSinceFirstPurchase;

    /** @var DateTime|null */
    protected $passwordAuthenticationDateTime;

    /** @var string|null */
    protected $cancellationPolicy;

    /** @var string|null */
    protected $purchaseCategory;

    /** @var string|null */
    protected $emailAccount;

    /** @var string|null */
    protected $mobileNetworkProvider;

    /** @var string|null */
    protected $applicationSourceOrBrowserType;

    /** @var bool|null */
    protected $confirmationDeviceMatchesAccount;

    /** @var LocationModel|null */
    protected $geographicLocation;

    /** @var int|null */
    protected $cardholderSinceVisitCount;

    /** @var DigitalReceiptModel|null */
    protected $digitalReceipt;

    /** @var int|null */
    protected $durationOfContract;

    /** @var string|null */
    protected $iccid;

    /** @var string|null */
    protected $reservationReceivedFrom;

    /** @var string|null */
    protected $deviceIpAddress;

    /** @var @var DateTime|null */
    protected $carRentalDropoffDate;

    /** @var string|null */
    protected $meiid;

    /** @var CreditInquiry|null */
    protected $creditInquiryResponse;

    /** @var bool|null */
    protected $rdrIntentToCredit = true;

    /**
     * @return bool|null
     */
    public function getCardCVV2ValidationAtRegistration()
    {
        return $this->cardCVV2ValidationAtRegistration;
    }

    /**
     * @param bool|null $cardCVV2ValidationAtRegistration
     */
    public function setCardCVV2ValidationAtRegistration($cardCVV2ValidationAtRegistration)
    {
        $this->cardCVV2ValidationAtRegistration = $cardCVV2ValidationAtRegistration;
    }

    /**
     * @return DateTime|null
     */
    public function getDateContractExecution()
    {
        return $this->dateContractExecution;
    }

    /**
     * @param DateTime|null $dateContractExecution
     */
    public function setDateContractExecution($dateContractExecution)
    {
        $this->dateContractExecution = $dateContractExecution;
    }

    /**
     * @return int|null
     */
    public function getAdditionalRelatedPurchases()
    {
        return $this->additionalRelatedPurchases;
    }

    /**
     * @param int|null $additionalRelatedPurchases
     */
    public function setAdditionalRelatedPurchases($additionalRelatedPurchases)
    {
        $this->additionalRelatedPurchases = $additionalRelatedPurchases;
    }

    /**
     * @return string|null
     */
    public function getLoyaltyNameRegistered()
    {
        return $this->loyaltyNameRegistered;
    }

    /**
     * @param string|null $loyaltyNameRegistered
     */
    public function setLoyaltyNameRegistered($loyaltyNameRegistered)
    {
        $this->loyaltyNameRegistered = $loyaltyNameRegistered;
    }

    /**
     * @return string|null
     */
    public function getFlightManifestName()
    {
        return $this->flightManifestName;
    }

    /**
     * @param string|null $flightManifestName
     */
    public function setFlightManifestName($flightManifestName)
    {
        $this->flightManifestName = $flightManifestName;
    }

    /**
     * @return string|null
     */
    public function getMerchantReferenceNumber()
    {
        return $this->merchantReferenceNumber;
    }

    /**
     * @param string|null $merchantReferenceNumber
     */
    public function setMerchantReferenceNumber($merchantReferenceNumber)
    {
        $this->merchantReferenceNumber = $merchantReferenceNumber;
    }

    /**
     * @return string|null
     */
    public function getCarRentalPickUpLocation()
    {
        return $this->carRentalPickUpLocation;
    }

    /**
     * @param string|null $carRentalPickUpLocation
     */
    public function setCarRentalPickUpLocation($carRentalPickUpLocation)
    {
        $this->carRentalPickUpLocation = $carRentalPickUpLocation;
    }

    /**
     * @return DateTime|null
     */
    public function getDateOfLastUtilization()
    {
        return $this->dateOfLastUtilization;
    }

    /**
     * @param DateTime|null $dateOfLastUtilization
     */
    public function setDateOfLastUtilization($dateOfLastUtilization)
    {
        $this->dateOfLastUtilization = $dateOfLastUtilization;
    }

    /**
     * @return bool|null
     */
    public function getCardCVV2ValidationAtPurchase()
    {
        return $this->cardCVV2ValidationAtPurchase;
    }

    /**
     * @param bool|null $cardCVV2ValidationAtPurchase
     */
    public function setCardCVV2ValidationAtPurchase($cardCVV2ValidationAtPurchase)
    {
        $this->cardCVV2ValidationAtPurchase = $cardCVV2ValidationAtPurchase;
    }

    /**
     * @return string|null
     */
    public function getSimDetails()
    {
        return $this->simDetails;
    }

    /**
     * @param string|null $simDetails
     */
    public function setSimDetails($simDetails)
    {
        $this->simDetails = $simDetails;
    }

    /**
     * @return string|null
     */
    public function getLinkToItemSold()
    {
        return $this->linkToItemSold;
    }

    /**
     * @param string|null $linkToItemSold
     */
    public function setLinkToItemSold($linkToItemSold)
    {
        $this->linkToItemSold = $linkToItemSold;
    }

    /**
     * @return DateTime|null
     */
    public function getDownloadCompletionDateTime()
    {
        return $this->downloadCompletionDateTime;
    }

    /**
     * @param DateTime|null $downloadCompletionDateTime
     */
    public function setDownloadCompletionDateTime($downloadCompletionDateTime)
    {
        $this->downloadCompletionDateTime = $downloadCompletionDateTime;
    }

    /**
     * @return string|null
     */
    public function getRecipientCustomerName()
    {
        return $this->recipientCustomerName;
    }

    /**
     * @param string|null $recipientCustomerName
     */
    public function setRecipientCustomerName($recipientCustomerName)
    {
        $this->recipientCustomerName = $recipientCustomerName;
    }

    /**
     * @return string|null
     */
    public function getDeliveryService()
    {
        return $this->deliveryService;
    }

    /**
     * @param string|null $deliveryService
     */
    public function setDeliveryService($deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    /**
     * @return float|null
     */
    public function getCardholderPriorVisitCount()
    {
        return $this->cardholderPriorVisitCount;
    }

    /**
     * @param float|null $cardholderPriorVisitCount
     */
    public function setCardholderPriorVisitCount($cardholderPriorVisitCount)
    {
        $this->cardholderPriorVisitCount = $cardholderPriorVisitCount;
    }

    /**
     * @return string|null
     */
    public function getWebsiteLink()
    {
        return $this->websiteLink;
    }

    /**
     * @param string|null $websiteLink
     */
    public function setWebsiteLink($websiteLink)
    {
        $this->websiteLink = $websiteLink;
    }

    /**
     * @return string|null
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * @param string|null $trackingNumber
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return CreditReimbursementInformationList|null
     */
    public function getCreditReimbursementInformationList()
    {
        return $this->creditReimbursementInformationList;
    }

    /**
     * @param CreditReimbursementInformationList|null $creditReimbursementInformationList
     */
    public function setCreditReimbursementInformationList($creditReimbursementInformationList)
    {
        $this->creditReimbursementInformationList = $creditReimbursementInformationList;
    }

    /**
     * @return bool|null
     */
    public function getAssuredCredit()
    {
        return $this->assuredCredit;
    }

    /**
     * @param bool|null $assuredCredit
     */
    public function setAssuredCredit($assuredCredit)
    {
        $this->assuredCredit = $assuredCredit;
    }

    /**
     * @return string|null
     */
    public function getConditionofGoods()
    {
        return $this->conditionofGoods;
    }

    /**
     * @param string|null $conditionofGoods
     */
    public function setConditionofGoods($conditionofGoods)
    {
        $this->conditionofGoods = $conditionofGoods;
    }

    /**
     * @return string|null
     */
    public function getCarRentalDropoffLocation()
    {
        return $this->carRentalDropoffLocation;
    }

    /**
     * @param string|null $carRentalDropoffLocation
     */
    public function setCarRentalDropoffLocation($carRentalDropoffLocation)
    {
        $this->carRentalDropoffLocation = $carRentalDropoffLocation;
    }

    /**
     * @return string|null
     */
    public function getAuthenticationConducted()
    {
        return $this->authenticationConducted;
    }

    /**
     * @param string|null $authenticationConducted
     */
    public function setAuthenticationConducted($authenticationConducted)
    {
        $this->authenticationConducted = $authenticationConducted;
    }

    /**
     * @return string|null
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }

    /**
     * @param string|null $deviceName
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;
    }

    /**
     * @return string|null
     */
    public function getLoyaltyNumberRegistered()
    {
        return $this->loyaltyNumberRegistered;
    }

    /**
     * @param string|null $loyaltyNumberRegistered
     */
    public function setLoyaltyNumberRegistered($loyaltyNumberRegistered)
    {
        $this->loyaltyNumberRegistered = $loyaltyNumberRegistered;
    }

    /**
     * @return int|null
     */
    public function getHotelRoomStayDuration()
    {
        return $this->hotelRoomStayDuration;
    }

    /**
     * @param int|null $hotelRoomStayDuration
     */
    public function setHotelRoomStayDuration($hotelRoomStayDuration)
    {
        $this->hotelRoomStayDuration = $hotelRoomStayDuration;
    }

    /**
     * @return string|null
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param string|null $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return string|null
     */
    public function getHotelRoomType()
    {
        return $this->hotelRoomType;
    }

    /**
     * @param string|null $hotelRoomType
     */
    public function setHotelRoomType($hotelRoomType)
    {
        $this->hotelRoomType = $hotelRoomType;
    }

    /**
     * @return DateTime|null
     */
    public function getDownloadDateTime()
    {
        return $this->downloadDateTime;
    }

    /**
     * @param DateTime|null $downloadDateTime
     */
    public function setDownloadDateTime($downloadDateTime)
    {
        $this->downloadDateTime = $downloadDateTime;
    }

    /**
     * @return string|null
     */
    public function getPackingSlip()
    {
        return $this->packingSlip;
    }

    /**
     * @param string|null $packingSlip
     */
    public function setPackingSlip($packingSlip)
    {
        $this->packingSlip = $packingSlip;
    }

    /**
     * @return int|null
     */
    public function getAdditionalRelatedDisputes()
    {
        return $this->additionalRelatedDisputes;
    }

    /**
     * @param int|null $additionalRelatedDisputes
     */
    public function setAdditionalRelatedDisputes($additionalRelatedDisputes)
    {
        $this->additionalRelatedDisputes = $additionalRelatedDisputes;
    }

    /**
     * @return string|null
     */
    public function getContactUs()
    {
        return $this->contactUs;
    }

    /**
     * @param string|null $contactUs
     */
    public function setContactUs($contactUs)
    {
        $this->contactUs = $contactUs;
    }

    /**
     * @return DateTime|null
     */
    public function getCarRentalPickUpDate()
    {
        return $this->carRentalPickUpDate;
    }

    /**
     * @param DateTime|null $carRentalPickUpDate
     */
    public function setCarRentalPickUpDate($carRentalPickUpDate)
    {
        $this->carRentalPickUpDate = $carRentalPickUpDate;
    }

    /**
     * @return AddressModel|null
     */
    public function getLoyaltyAddressRegistered()
    {
        return $this->loyaltyAddressRegistered;
    }

    /**
     * @param AddressModel|null $loyaltyAddressRegistered
     */
    public function setLoyaltyAddressRegistered($loyaltyAddressRegistered)
    {
        $this->loyaltyAddressRegistered = $loyaltyAddressRegistered;
    }

    /**
     * @return int|null
     */
    public function getCustomerNameDuration()
    {
        return $this->customerNameDuration;
    }

    /**
     * @param int|null $customerNameDuration
     */
    public function setCustomerNameDuration($customerNameDuration)
    {
        $this->customerNameDuration = $customerNameDuration;
    }

    /**
     * @return ShippingInfoModel
     */
    public function getShippingInfo()
    {
        return $this->shippingInfo;
    }

    /**
     * @param ShippingInfoModel $shippingInfo
     */
    public function setShippingInfo($shippingInfo)
    {
        $this->shippingInfo = $shippingInfo;
    }

    /**
     * @return LocationModel|null
     */
    public function getStoreLocation()
    {
        return $this->storeLocation;
    }

    /**
     * @param LocationModel|null $storeLocation
     */
    public function setStoreLocation($storeLocation)
    {
        $this->storeLocation = $storeLocation;
    }

    /**
     * @return string|null
     */
    public function getContractFirstPaymentDate()
    {
        return $this->contractFirstPaymentDate;
    }

    /**
     * @param string|null $contractFirstPaymentDate
     */
    public function setContractFirstPaymentDate($contractFirstPaymentDate)
    {
        $this->contractFirstPaymentDate = $contractFirstPaymentDate;
    }

    /**
     * @return bool|null
     */
    public function getContractFirstPaymentDisputed()
    {
        return $this->contractFirstPaymentDisputed;
    }

    /**
     * @param bool|null $contractFirstPaymentDisputed
     */
    public function setContractFirstPaymentDisputed($contractFirstPaymentDisputed)
    {
        $this->contractFirstPaymentDisputed = $contractFirstPaymentDisputed;
    }

    /**
     * @return AddressModel|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param AddressModel|null $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getPassengerName4()
    {
        return $this->passengerName4;
    }

    /**
     * @param string|null $passengerName4
     */
    public function setPassengerName4($passengerName4)
    {
        $this->passengerName4 = $passengerName4;
    }

    /**
     * @return DateTime|null
     */
    public function getDownloadStartDateTime()
    {
        return $this->downloadStartDateTime;
    }

    /**
     * @param DateTime|null $downloadStartDateTime
     */
    public function setDownloadStartDateTime($downloadStartDateTime)
    {
        $this->downloadStartDateTime = $downloadStartDateTime;
    }

    /**
     * @return string|null
     */
    public function getRelationshipToAccountholder()
    {
        return $this->relationshipToAccountholder;
    }

    /**
     * @param string|null $relationshipToAccountholder
     */
    public function setRelationshipToAccountholder($relationshipToAccountholder)
    {
        $this->relationshipToAccountholder = $relationshipToAccountholder;
    }

    /**
     * @return string|null
     */
    public function getReservationNumber()
    {
        return $this->reservationNumber;
    }

    /**
     * @param string|null $reservationNumber
     */
    public function setReservationNumber($reservationNumber)
    {
        $this->reservationNumber = $reservationNumber;
    }

    /**
     * @return float|null
     */
    public function getEmailAccountHistory()
    {
        return $this->emailAccountHistory;
    }

    /**
     * @param float|null $emailAccountHistory
     */
    public function setEmailAccountHistory($emailAccountHistory)
    {
        $this->emailAccountHistory = $emailAccountHistory;
    }

    /**
     * @return int|null
     */
    public function getCountOfPurchasesSinceRegistration()
    {
        return $this->countOfPurchasesSinceRegistration;
    }

    /**
     * @param int|null $countOfPurchasesSinceRegistration
     */
    public function setCountOfPurchasesSinceRegistration($countOfPurchasesSinceRegistration)
    {
        $this->countOfPurchasesSinceRegistration = $countOfPurchasesSinceRegistration;
    }

    /**
     * @return DateTime|null
     */
    public function getPasswordLastUpdatedDateTime()
    {
        return $this->passwordLastUpdatedDateTime;
    }

    /**
     * @param DateTime|null $passwordLastUpdatedDateTime
     */
    public function setPasswordLastUpdatedDateTime($passwordLastUpdatedDateTime)
    {
        $this->passwordLastUpdatedDateTime = $passwordLastUpdatedDateTime;
    }

    /**
     * @return DateTime|null
     */
    public function getFlightManifestDateOfBirth()
    {
        return $this->flightManifestDateOfBirth;
    }

    /**
     * @param DateTime|null $flightManifestDateOfBirth
     */
    public function setFlightManifestDateOfBirth($flightManifestDateOfBirth)
    {
        $this->flightManifestDateOfBirth = $flightManifestDateOfBirth;
    }

    /**
     * @return string|null
     */
    public function getSpecialInstructions()
    {
        return $this->specialInstructions;
    }

    /**
     * @param string|null $specialInstructions
     */
    public function setSpecialInstructions($specialInstructions)
    {
        $this->specialInstructions = $specialInstructions;
    }

    /**
     * @return string|null
     */
    public function getCarRentalVehicleType()
    {
        return $this->carRentalVehicleType;
    }

    /**
     * @param string|null $carRentalVehicleType
     */
    public function setCarRentalVehicleType($carRentalVehicleType)
    {
        $this->carRentalVehicleType = $carRentalVehicleType;
    }

    /**
     * @return string|null
     */
    public function getTravelInsurance()
    {
        return $this->travelInsurance;
    }

    /**
     * @param string|null $travelInsurance
     */
    public function setTravelInsurance($travelInsurance)
    {
        $this->travelInsurance = $travelInsurance;
    }

    /**
     * @return bool|null
     */
    public function getTravelUtilizationIndicator()
    {
        return $this->travelUtilizationIndicator;
    }

    /**
     * @param bool|null $travelUtilizationIndicator
     */
    public function setTravelUtilizationIndicator($travelUtilizationIndicator)
    {
        $this->travelUtilizationIndicator = $travelUtilizationIndicator;
    }

    /**
     * @return DateTime|null
     */
    public function getDateofReservation()
    {
        return $this->dateofReservation;
    }

    /**
     * @param DateTime|null $dateofReservation
     */
    public function setDateofReservation($dateofReservation)
    {
        $this->dateofReservation = $dateofReservation;
    }

    /**
     * @return string|null
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * @param string|null $deliveryStatus
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;
    }

    /**
     * @return string|null
     */
    public function getCommunications()
    {
        return $this->communications;
    }

    /**
     * @param string|null $communications
     */
    public function setCommunications($communications)
    {
        $this->communications = $communications;
    }

    /**
     * @return string|null
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param string|null $deviceType
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
    }

    /**
     * @return string|null
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param string|null $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return string|null
     */
    public function getItinerary()
    {
        return $this->itinerary;
    }

    /**
     * @param string|null $itinerary
     */
    public function setItinerary($itinerary)
    {
        $this->itinerary = $itinerary;
    }

    /**
     * @return AddressModel|null
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param AddressModel|null $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return int|null
     */
    public function getCountOfDisputedPurchasesSinceRegistration()
    {
        return $this->countOfDisputedPurchasesSinceRegistration;
    }

    /**
     * @param int|null $countOfDisputedPurchasesSinceRegistration
     */
    public function setCountOfDisputedPurchasesSinceRegistration($countOfDisputedPurchasesSinceRegistration)
    {
        $this->countOfDisputedPurchasesSinceRegistration = $countOfDisputedPurchasesSinceRegistration;
    }

    /**
     * @return float|null
     */
    public function getMonthsSinceFirstPurchase()
    {
        return $this->monthsSinceFirstPurchase;
    }

    /**
     * @param float|null $monthsSinceFirstPurchase
     */
    public function setMonthsSinceFirstPurchase($monthsSinceFirstPurchase)
    {
        $this->monthsSinceFirstPurchase = $monthsSinceFirstPurchase;
    }

    /**
     * @return DateTime|null
     */
    public function getPasswordAuthenticationDateTime()
    {
        return $this->passwordAuthenticationDateTime;
    }

    /**
     * @param DateTime|null $passwordAuthenticationDateTime
     */
    public function setPasswordAuthenticationDateTime($passwordAuthenticationDateTime)
    {
        $this->passwordAuthenticationDateTime = $passwordAuthenticationDateTime;
    }

    /**
     * @return string|null
     */
    public function getCancellationPolicy()
    {
        return $this->cancellationPolicy;
    }

    /**
     * @param string|null $cancellationPolicy
     */
    public function setCancellationPolicy($cancellationPolicy)
    {
        $this->cancellationPolicy = $cancellationPolicy;
    }

    /**
     * @return string|null
     */
    public function getPassengerName5()
    {
        return $this->passengerName5;
    }

    /**
     * @param string|null $passengerName5
     */
    public function setPassengerName5($passengerName5)
    {
        $this->passengerName5 = $passengerName5;
    }

    /**
     * @return string|null
     */
    public function getPurchaseCategory()
    {
        return $this->purchaseCategory;
    }

    /**
     * @param string|null $purchaseCategory
     */
    public function setPurchaseCategory($purchaseCategory)
    {
        $this->purchaseCategory = $purchaseCategory;
    }

    /**
     * @return string|null
     */
    public function getEmailAccount()
    {
        return $this->emailAccount;
    }

    /**
     * @param string|null $emailAccount
     */
    public function setEmailAccount($emailAccount)
    {
        $this->emailAccount = $emailAccount;
    }

    /**
     * @return string|null
     */
    public function getMobileNetworkProvider()
    {
        return $this->mobileNetworkProvider;
    }

    /**
     * @param string|null $mobileNetworkProvider
     */
    public function setMobileNetworkProvider($mobileNetworkProvider)
    {
        $this->mobileNetworkProvider = $mobileNetworkProvider;
    }

    /**
     * @return string|null
     */
    public function getPassengerName1()
    {
        return $this->passengerName1;
    }

    /**
     * @param string|null $passengerName1
     */
    public function setPassengerName1($passengerName1)
    {
        $this->passengerName1 = $passengerName1;
    }

    /**
     * @return string|null
     */
    public function getPassengerName3()
    {
        return $this->passengerName3;
    }

    /**
     * @param string|null $passengerName3
     */
    public function setPassengerName3($passengerName3)
    {
        $this->passengerName3 = $passengerName3;
    }

    /**
     * @return string|null
     */
    public function getPassengerName2()
    {
        return $this->passengerName2;
    }

    /**
     * @param string|null $passengerName2
     */
    public function setPassengerName2($passengerName2)
    {
        $this->passengerName2 = $passengerName2;
    }

    /**
     * @return string|null
     */
    public function getApplicationSourceOrBrowserType()
    {
        return $this->applicationSourceOrBrowserType;
    }

    /**
     * @param string|null $applicationSourceOrBrowserType
     */
    public function setApplicationSourceOrBrowserType($applicationSourceOrBrowserType)
    {
        $this->applicationSourceOrBrowserType = $applicationSourceOrBrowserType;
    }

    /**
     * @return bool|null
     */
    public function getConfirmationDeviceMatchesAccount()
    {
        return $this->confirmationDeviceMatchesAccount;
    }

    /**
     * @param bool|null $confirmationDeviceMatchesAccount
     */
    public function setConfirmationDeviceMatchesAccount($confirmationDeviceMatchesAccount)
    {
        $this->confirmationDeviceMatchesAccount = $confirmationDeviceMatchesAccount;
    }

    /**
     * @return LocationModel|null
     */
    public function getGeographicLocation()
    {
        return $this->geographicLocation;
    }

    /**
     * @param LocationModel|null $geographicLocation
     */
    public function setGeographicLocation($geographicLocation)
    {
        $this->geographicLocation = $geographicLocation;
    }

    /**
     * @return float|null
     */
    public function getCardholderSinceVisitCount()
    {
        return $this->cardholderSinceVisitCount;
    }

    /**
     * @param float|null $cardholderSinceVisitCount
     */
    public function setCardholderSinceVisitCount($cardholderSinceVisitCount)
    {
        $this->cardholderSinceVisitCount = $cardholderSinceVisitCount;
    }

    /**
     * @return DigitalReceiptModel|null
     */
    public function getDigitalReceipt()
    {
        return $this->digitalReceipt;
    }

    /**
     * @param DigitalReceiptModel|null $digitalReceipt
     */
    public function setDigitalReceipt($digitalReceipt)
    {
        $this->digitalReceipt = $digitalReceipt;
    }

    /**
     * @return float|null
     */
    public function getDurationOfContract()
    {
        return $this->durationOfContract;
    }

    /**
     * @param float|null $durationOfContract
     */
    public function setDurationOfContract($durationOfContract)
    {
        $this->durationOfContract = $durationOfContract;
    }

    /**
     * @return string|null
     */
    public function getIccid()
    {
        return $this->iccid;
    }

    /**
     * @param string|null $iccid
     */
    public function setIccid($iccid)
    {
        $this->iccid = $iccid;
    }

    /**
     * @return string|null
     */
    public function getReservationReceivedFrom()
    {
        return $this->reservationReceivedFrom;
    }

    /**
     * @param string|null $reservationReceivedFrom
     */
    public function setReservationReceivedFrom($reservationReceivedFrom)
    {
        $this->reservationReceivedFrom = $reservationReceivedFrom;
    }

    /**
     * @return string|null
     */
    public function getDeviceIpAddress()
    {
        return $this->deviceIpAddress;
    }

    /**
     * @param string|null $deviceIpAddress
     */
    public function setDeviceIpAddress($deviceIpAddress)
    {
        $this->deviceIpAddress = $deviceIpAddress;
    }

    /**
     * @return DateTime|null
     */
    public function getCarRentalDropoffDate()
    {
        return $this->carRentalDropoffDate;
    }

    /**
     * @param DateTime|null $carRentalDropoffDate
     */
    public function setCarRentalDropoffDate($carRentalDropoffDate)
    {
        $this->carRentalDropoffDate = $carRentalDropoffDate;
    }

    /**
     * @return string|null
     */
    public function getMeiid()
    {
        return $this->meiid;
    }

    /**
     * @param string|null $meiid
     */
    public function setMeiid($meiid)
    {
        $this->meiid = $meiid;
    }

    /**
     * @return CreditInquiry|null
     */
    public function getCreditInquiryResponse()
    {
        return $this->creditInquiryResponse;
    }

    /**
     * @param CreditInquiry|null $creditInquiryResponse
     */
    public function setCreditInquiryResponse($creditInquiryResponse)
    {
        $this->creditInquiryResponse = $creditInquiryResponse;
    }

    /**
     * @return bool|null
     */
    public function isRdrIntentToCredit()
    {
        return $this->rdrIntentToCredit;
    }

    /**
     * @param bool|null $rdrIntentToCredit
     */
    public function setRdrIntentToCredit($rdrIntentToCredit)
    {
        $this->rdrIntentToCredit = $rdrIntentToCredit;
    }
}
