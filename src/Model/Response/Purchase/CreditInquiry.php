<?php namespace Alerthit\Model\Response\Purchase;

use Alerthit\Model\AmountModel;

class CreditInquiry
{
    /**
     * Visa-generated identifier that is unique for each original transaction. The transaction identifier (TID) is a key
     * element that links original authorization requests to subsequent messages, such as credits.
     * Valid numeric with maxlength of 15.
     * pattern [0-9]{15}
     *
     * @var string|null
     */
    protected $creditTransactionId;

    /**
     * Date and time as to when the credit happened.
     *
     * @var \DateTime|null
     */
    protected $creditDateAndTime;

    /**
     * @var AmountModel|null
     */
    protected $creditAmount;

    /**
     * @var string|null
     */
    protected $merchantName;

    /**
     * Valid alphanumeric with maximum length of 6.
     *
     * @var string|null
     */
    protected $authorizationCode;

    /**
     * 12 digit Retrieval Reference Number.
     * maxLength 12
     *
     * @var string|null
     */
    protected $rrn;

    /**
     * Acquirer Reference Number
     * A valid ARN with length between 23 and 24.
     * pattern [0-9]{23}|[0-9]{24}
     *
     * @var string|null
     */
    protected $arn;

    /**
     * @return string|null
     */
    public function getCreditTransactionId()
    {
        return $this->creditTransactionId;
    }

    /**
     * @param string|null $creditTransactionId
     */
    public function setCreditTransactionId($creditTransactionId)
    {
        $this->creditTransactionId = $creditTransactionId;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreditDateAndTime()
    {
        return $this->creditDateAndTime;
    }

    /**
     * @param \DateTime|null $creditDateAndTime
     */
    public function setCreditDateAndTime($creditDateAndTime)
    {
        $this->creditDateAndTime = $creditDateAndTime;
    }

    /**
     * @return AmountModel|null
     */
    public function getCreditAmount()
    {
        return $this->creditAmount;
    }

    /**
     * @param AmountModel|null $creditAmount
     */
    public function setCreditAmount($creditAmount)
    {
        $this->creditAmount = $creditAmount;
    }

    /**
     * @return string|null
     */
    public function getMerchantName()
    {
        return $this->merchantName;
    }

    /**
     * @param string|null $merchantName
     */
    public function setMerchantName($merchantName)
    {
        $this->merchantName = $merchantName;
    }

    /**
     * @return string|null
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * @param string|null $authorizationCode
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;
    }

    /**
     * @return string|null
     */
    public function getRrn()
    {
        return $this->rrn;
    }

    /**
     * @param string|null $rrn
     */
    public function setRrn($rrn)
    {
        $this->rrn = $rrn;
    }

    /**
     * @return string|null
     */
    public function getArn()
    {
        return $this->arn;
    }

    /**
     * @param string|null $arn
     */
    public function setArn($arn)
    {
        $this->arn = $arn;
    }
}
