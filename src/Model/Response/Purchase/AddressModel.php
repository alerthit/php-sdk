<?php namespace Alerthit\Model\Response\Purchase;

class AddressModel
{
    /** @var string|null */
    protected $city;

    /** @var string|null */
    protected $subEntityCode;

    /** @var string|null */
    protected $address1;

    /** @var string|null */
    protected $address2;

    /** @var string|null */
    protected $state;

    /** @var string|null */
    protected $country;

    /** @var string|null */
    protected $postalCode;

    /**
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getSubEntityCode()
    {
        return $this->subEntityCode;
    }

    /**
     * @param string|null $subEntityCode
     */
    public function setSubEntityCode($subEntityCode)
    {
        $this->subEntityCode = $subEntityCode;
    }

    /**
     * @return string|null
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return string|null
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }
}
