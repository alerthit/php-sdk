<?php namespace Alerthit\Model\Response\Purchase;

class LocationModel
{
    /** @var float|null */
    protected $latitude;

    /** @var float|null */
    protected $longitude;

    /** @var @var AddressModel|null */
    protected $address;

    /**
     * @return float|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float|null $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float|null $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return AddressModel|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param AddressModel|null $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}
