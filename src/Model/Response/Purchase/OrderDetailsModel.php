<?php namespace Alerthit\Model\Response\Purchase;

class OrderDetailsModel
{
    /** @var ItemModel[]|null */
    protected $item;

    /**
     * @return ItemModel[]|null
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param ItemModel[]|null $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }
}
