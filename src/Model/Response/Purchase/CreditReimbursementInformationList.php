<?php namespace Alerthit\Model\Response\Purchase;

class CreditReimbursementInformationList
{
    /** @var CreditReimbursementInformation|null  */
    protected $creditReimbursementInformation;

    /**
     * @param CreditReimbursementInformation|null $creditReimbursementInformation
     */
    public function __construct($creditReimbursementInformation)
    {
        $this->creditReimbursementInformation = $creditReimbursementInformation;
    }

    /**
     * @return CreditReimbursementInformation|null
     */
    public function getCreditReimbursementInformation()
    {
        return $this->creditReimbursementInformation;
    }

    /**
     * @param CreditReimbursementInformation|null $creditReimbursementInformation
     */
    public function setCreditReimbursementInformation($creditReimbursementInformation)
    {
        $this->creditReimbursementInformation = $creditReimbursementInformation;
    }
}
