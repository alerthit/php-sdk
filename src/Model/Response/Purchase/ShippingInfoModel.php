<?php namespace Alerthit\Model\Response\Purchase;

class ShippingInfoModel
{
    /** @var string|null */
    protected $deliveryStatus;

    /** @var string|null */
    protected $trackingNumber;

    /** @var string|null */
    protected $otherStatusDescription;

    /** @var string|null */
    protected $shippedDate;

    /** @var string|null */
    protected $deliveryService;

    /**
     * @return string|null
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * @param string|null $deliveryStatus
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;
    }

    /**
     * @return string|null
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * @param string|null $trackingNumber
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return string|null
     */
    public function getOtherStatusDescription()
    {
        return $this->otherStatusDescription;
    }

    /**
     * @param string|null $otherStatusDescription
     */
    public function setOtherStatusDescription($otherStatusDescription)
    {
        $this->otherStatusDescription = $otherStatusDescription;
    }

    /**
     * @return string|null
     */
    public function getShippedDate()
    {
        return $this->shippedDate;
    }

    /**
     * @param string|null $shippedDate
     */
    public function setShippedDate($shippedDate)
    {
        $this->shippedDate = $shippedDate;
    }

    /**
     * @return string|null
     */
    public function getDeliveryService()
    {
        return $this->deliveryService;
    }

    /**
     * @param string|null $deliveryService
     */
    public function setDeliveryService($deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }
}
