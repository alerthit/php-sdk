<?php namespace Alerthit\Model\Response\Purchase;

use Alerthit\Model\AmountModel;

class ItemModel
{
    /** @var string|null */
    protected $itemType;

    /** @var AmountModel|null */
    protected $price;

    /** @var string|null */
    protected $itemDescription;

    /** @var ShippingInfoModel|null */
    protected $shippingInfo;

    /** @var string|null */
    protected $artistOrSeller;

    /** @var int|null */
    protected $quantity;

    /**
     * @return string|null
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * @param string|null $itemType
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;
    }

    /**
     * @return AmountModel|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param AmountModel|null $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     * @param string|null $itemDescription
     */
    public function setItemDescription($itemDescription)
    {
        $this->itemDescription = $itemDescription;
    }

    /**
     * @return ShippingInfoModel|null
     */
    public function getShippingInfo()
    {
        return $this->shippingInfo;
    }

    /**
     * @param ShippingInfoModel|null $shippingInfo
     */
    public function setShippingInfo($shippingInfo)
    {
        $this->shippingInfo = $shippingInfo;
    }

    /**
     * @return string|null
     */
    public function getArtistOrSeller()
    {
        return $this->artistOrSeller;
    }

    /**
     * @param string|null $artistOrSeller
     */
    public function setArtistOrSeller($artistOrSeller)
    {
        $this->artistOrSeller = $artistOrSeller;
    }

    /**
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}
