<?php namespace Alerthit\Model\Response\Purchase;

use Alerthit\Model\AmountModel;
use DateTime;

class CreditReimbursementInformation
{
    /** @var int|null */
    protected $creditReimbursementSequenceNumber;

    /** @var @var DateTime|null */
    protected $creditReimbursementDate;

    /** @var string|null */
    protected $creditReimbursementMethod;

    /** @var string|null */
    protected $voucherDetails;

    /** @var DateTime|null */
    protected $voucherExpirationDate;

    /** @var AmountModel|null */
    protected $voucherAmount;

    /** @var DateTime|null */
    protected $voucherDate;

    /** @var string|null */
    protected $voucherReference;

    /** @var AmountModel|null */
    protected $creditAmount;

    /** @var DateTime|null */
    protected $creditDate;

    /** @var string|null */
    protected $creditReference;

    /**
     * @return int|null
     */
    public function getCreditReimbursementSequenceNumber()
    {
        return $this->creditReimbursementSequenceNumber;
    }

    /**
     * @param int|null $creditReimbursementSequenceNumber
     */
    public function setCreditReimbursementSequenceNumber($creditReimbursementSequenceNumber)
    {
        $this->creditReimbursementSequenceNumber = $creditReimbursementSequenceNumber;
    }

    /**
     * @return mixed
     */
    public function getCreditReimbursementDate()
    {
        return $this->creditReimbursementDate;
    }

    /**
     * @param mixed $creditReimbursementDate
     */
    public function setCreditReimbursementDate($creditReimbursementDate)
    {
        $this->creditReimbursementDate = $creditReimbursementDate;
    }

    /**
     * @return string|null
     */
    public function getCreditReimbursementMethod()
    {
        return $this->creditReimbursementMethod;
    }

    /**
     * @param string|null $creditReimbursementMethod
     */
    public function setCreditReimbursementMethod($creditReimbursementMethod)
    {
        $this->creditReimbursementMethod = $creditReimbursementMethod;
    }

    /**
     * @return string|null
     */
    public function getVoucherDetails()
    {
        return $this->voucherDetails;
    }

    /**
     * @param string|null $voucherDetails
     */
    public function setVoucherDetails($voucherDetails)
    {
        $this->voucherDetails = $voucherDetails;
    }

    /**
     * @return DateTime|null
     */
    public function getVoucherExpirationDate()
    {
        return $this->voucherExpirationDate;
    }

    /**
     * @param DateTime|null $voucherExpirationDate
     */
    public function setVoucherExpirationDate($voucherExpirationDate)
    {
        $this->voucherExpirationDate = $voucherExpirationDate;
    }

    /**
     * @return AmountModel|null
     */
    public function getVoucherAmount()
    {
        return $this->voucherAmount;
    }

    /**
     * @param AmountModel|null $voucherAmount
     */
    public function setVoucherAmount($voucherAmount)
    {
        $this->voucherAmount = $voucherAmount;
    }

    /**
     * @return DateTime|null
     */
    public function getVoucherDate()
    {
        return $this->voucherDate;
    }

    /**
     * @param DateTime|null $voucherDate
     */
    public function setVoucherDate($voucherDate)
    {
        $this->voucherDate = $voucherDate;
    }

    /**
     * @return string|null
     */
    public function getVoucherReference()
    {
        return $this->voucherReference;
    }

    /**
     * @param string|null $voucherReference
     */
    public function setVoucherReference($voucherReference)
    {
        $this->voucherReference = $voucherReference;
    }

    /**
     * @return AmountModel|null
     */
    public function getCreditAmount()
    {
        return $this->creditAmount;
    }

    /**
     * @param AmountModel|null $creditAmount
     */
    public function setCreditAmount($creditAmount)
    {
        $this->creditAmount = $creditAmount;
    }

    /**
     * @return DateTime|null
     */
    public function getCreditDate()
    {
        return $this->creditDate;
    }

    /**
     * @param DateTime|null $creditDate
     */
    public function setCreditDate($creditDate)
    {
        $this->creditDate = $creditDate;
    }

    /**
     * @return string|null
     */
    public function getCreditReference()
    {
        return $this->creditReference;
    }

    /**
     * @param string|null $creditReference
     */
    public function setCreditReference($creditReference)
    {
        $this->creditReference = $creditReference;
    }
}
