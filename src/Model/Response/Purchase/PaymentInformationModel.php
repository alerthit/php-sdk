<?php namespace Alerthit\Model\Response\Purchase;

use Alerthit\Model\AmountModel;

class PaymentInformationModel
{
    /** @var AmountModel|null */
    protected $itemsSubTotal;

    /** @var AmountModel|null */
    protected $tax;

    /** @var AmountModel|null */
    protected $totalBeforeTax;

    /** @var string|null */
    protected $billingName;

    /** @var AddressModel|null */
    protected $billingAddress;

    /** @var AmountModel|null */
    protected $total;

    /** @var string|null */
    protected $paymentMethod;

    /** @var string|null */
    protected $taxDescription;

    /**
     * @return AmountModel|null
     */
    public function getItemsSubTotal()
    {
        return $this->itemsSubTotal;
    }

    /**
     * @param AmountModel|null $itemsSubTotal
     */
    public function setItemsSubTotal($itemsSubTotal)
    {
        $this->itemsSubTotal = $itemsSubTotal;
    }

    /**
     * @return AmountModel|null
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param AmountModel|null $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return AmountModel|null
     */
    public function getTotalBeforeTax()
    {
        return $this->totalBeforeTax;
    }

    /**
     * @param AmountModel|null $totalBeforeTax
     */
    public function setTotalBeforeTax($totalBeforeTax)
    {
        $this->totalBeforeTax = $totalBeforeTax;
    }

    /**
     * @return string|null
     */
    public function getBillingName()
    {
        return $this->billingName;
    }

    /**
     * @param string|null $billingName
     */
    public function setBillingName($billingName)
    {
        $this->billingName = $billingName;
    }

    /**
     * @return AddressModel|null
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param AddressModel|null $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return AmountModel|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param AmountModel|null $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string|null
     */
    public function getTaxDescription()
    {
        return $this->taxDescription;
    }

    /**
     * @param string|null $taxDescription
     */
    public function setTaxDescription($taxDescription)
    {
        $this->taxDescription = $taxDescription;
    }
}
