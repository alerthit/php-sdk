<?php namespace Alerthit\Model\Response\Purchase;

use Alerthit\Model\AmountModel;

class DigitalReceiptModel
{
    /** @var string|null */
    protected $orderNumber;

    /** @var AmountModel|null */
    protected $orderTotal;

    /** @var AmountModel|null */
    protected $tax;

    /** @var PaymentInformationModel|null */
    protected $paymentInformation;

    /** @var string|null */
    protected $orderDateTime;

    /** @var AmountModel|null */
    protected $shippingAndHandlingCharge;

    /** @var string|null */
    protected $titlesIncludedInOrder;

    /** @var OrderDetailsModel|null */
    protected $orderDetails;

    /** @var string|null */
    protected $invoiceNumber;

    /** @var AmountModel|null */
    protected $subTotal;

    /** @var string|null */
    protected $taxDescription;

    /**
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string|null $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return AmountModel|null
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * @param AmountModel|null $orderTotal
     */
    public function setOrderTotal($orderTotal)
    {
        $this->orderTotal = $orderTotal;
    }

    /**
     * @return AmountModel|null
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param AmountModel|null $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return PaymentInformationModel|null
     */
    public function getPaymentInformation()
    {
        return $this->paymentInformation;
    }

    /**
     * @param PaymentInformationModel|null $paymentInformation
     */
    public function setPaymentInformation($paymentInformation)
    {
        $this->paymentInformation = $paymentInformation;
    }

    /**
     * @return string|null
     */
    public function getOrderDateTime()
    {
        return $this->orderDateTime;
    }

    /**
     * @param string|null $orderDateTime
     */
    public function setOrderDateTime($orderDateTime)
    {
        $this->orderDateTime = $orderDateTime;
    }

    /**
     * @return AmountModel|null
     */
    public function getShippingAndHandlingCharge()
    {
        return $this->shippingAndHandlingCharge;
    }

    /**
     * @param AmountModel|null $shippingAndHandlingCharge
     */
    public function setShippingAndHandlingCharge($shippingAndHandlingCharge)
    {
        $this->shippingAndHandlingCharge = $shippingAndHandlingCharge;
    }

    /**
     * @return string|null
     */
    public function getTitlesIncludedInOrder()
    {
        return $this->titlesIncludedInOrder;
    }

    /**
     * @param string|null $titlesIncludedInOrder
     */
    public function setTitlesIncludedInOrder($titlesIncludedInOrder)
    {
        $this->titlesIncludedInOrder = $titlesIncludedInOrder;
    }

    /**
     * @return OrderDetailsModel|null
     */
    public function getOrderDetails()
    {
        return $this->orderDetails;
    }

    /**
     * @param OrderDetailsModel|null $orderDetails
     */
    public function setOrderDetails($orderDetails)
    {
        $this->orderDetails = $orderDetails;
    }

    /**
     * @return string|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string|null $invoiceNumber
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    /**
     * @return AmountModel|null
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param AmountModel|null $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return string|null
     */
    public function getTaxDescription()
    {
        return $this->taxDescription;
    }

    /**
     * @param string|null $taxDescription
     */
    public function setTaxDescription($taxDescription)
    {
        $this->taxDescription = $taxDescription;
    }
}
