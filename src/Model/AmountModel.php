<?php namespace Alerthit\Model;

class AmountModel
{
    /**
     * @var float|null
     */
    protected $value;

    /**
     * @var string|null
     */
    protected $currency;

    public function __construct($value = null, $currency = null)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * @return float|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float|null $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
}
