<?php namespace Alerthit;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Throwable;

class Api
{
    /** @var Client */
    protected $client;

    /** @var string */
    private $requestIdentifier;

    /** @var string */
    private $privateKey;

    /** @var Throwable */
    private $exception;

    public function __construct(
        $requestIdentifier,
        $privateKey,
        $baseUri
    ) {
        $this->requestIdentifier = $requestIdentifier;
        $this->privateKey = $privateKey;

        $this->client = new Client([
            'base_uri' => $baseUri,
            'verify'   => true,
        ]);
    }

    /**
     * @param array $attributes
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function validateResponse(array $attributes)
    {
        return $this->sendRequest('validate-response', $attributes);
    }

    /**
     * @param array $attributes
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function createTestAlert(array $attributes)
    {
        return $this->sendRequest('create-test-alert', $attributes);
    }

    /**
     * @return string
     */
    public function getRequestIdentifier()
    {
        return $this->requestIdentifier;
    }

    /**
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @return Throwable
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param string $method
     * @param array  $attributes
     *
     * @return mixed
     */
    public function sendRequest($method, $attributes)
    {
        $request = $this->makeRequest($method, $attributes);

        try {
            $response = $this->client->send($request);

            $responseBody = $response->getBody()->getContents();

            return json_decode($responseBody, true);
        } catch (\Exception $e) {
            $this->exception = $e;

            return [
                'status' => 'error',
                'error'  => [
                    'code'           => '0.00',
                    'exception_code' => $e->getCode(),
                    'messages'       => [
                        'Unexpected error',
                        sprintf('Caught %s exception.', get_class($e)),
                        $e->getMessage(),
                    ],
                ],
            ];
        }
    }

    /**
     * @param string $path
     * @param array  $attributes
     *
     * @return Request
     */
    private function makeRequest($path, $attributes)
    {
        $data = json_encode($attributes);

        return new Request(
            'POST', $path, [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'Account'      => $this->getRequestIdentifier(),
            'Signature'    => $this->generateSignature($data),
        ], $data
        );
    }

    /**
     * @param $data
     *
     * @return string
     */
    private function generateSignature($data)
    {
        return base64_encode(
            hash_hmac('sha512',
                $this->getRequestIdentifier() . $data . $this->getRequestIdentifier(),
                $this->getPrivateKey())
        );
    }
}
