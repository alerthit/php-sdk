<?php

namespace Model\Request;

use Alerthit\Model\Request\AlertModel;
use PHPUnit\Framework\TestCase;

class AlertModelTest extends TestCase
{
    public function testCreateAlertModel()
    {
        $content = '{
  "alertId": "334d65ed-e777-45a0-a196-a9fa1d5e881b",
  "requestHeader": {
    "user": "mohja1056e",
    "callType": [
      "1"
    ]
  },
  "requestData": {
    "personalAccountNumber": "446278XXXXXX7422",
    "transactionDate": "2019-07-23T00:00:00.000",
    "authorizationDateTime": "2019-07-24T00:00:00.000",
    "transactionAmount": {
      "value": "2.99",
      "currency": "826"
    },
    "arn": "74208329205019180863251",
    "authorizationCode": "023303",
    "cardAcceptorId": "72000362",
    "purchaseIdentifier": "",
    "transactionID": "489204674515371",
    "transactionType": "SALE",
    "descriptor": "google.com"
  }
}';

        $alertModel = new AlertModel(json_decode($content, true));

        $this->assertEquals(true, $alertModel->getRequestHeader()->isCallTypeInquiry());
        $this->assertEquals(false, $alertModel->getRequestHeader()->isCallTypeInitRefund());
        $this->assertEquals('489204674515371', $alertModel->getRequestData()->getTransactionId());
        $this->assertEquals('74208329205019180863251', $alertModel->getRequestData()->getArn());
        $this->assertEquals('023303', $alertModel->getRequestData()->getAuthorizationCode());
        $this->assertEquals('446278XXXXXX7422', $alertModel->getRequestData()->getPersonalAccountNumber());
        $this->assertEquals('2019-07-23T00:00:00.000', $alertModel->getRequestData()->getTransactionDate());
        $this->assertEquals('2.99', $alertModel->getRequestData()->getTransactionAmount()->getValue());
        $this->assertEquals('826', $alertModel->getRequestData()->getTransactionAmount()->getCurrency());
        $this->assertEquals('SALE', $alertModel->getRequestData()->getTransactionType());
        $this->assertEquals('', $alertModel->getRequestData()->getPurchaseIdentifier());
        $this->assertEquals('72000362', $alertModel->getRequestData()->getCardAcceptorId());
    }
}
